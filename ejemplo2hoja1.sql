﻿DROP DATABASE IF EXISTS b20190529;
CREATE DATABASE b20190529;
USE b20190529;

/* CREANDO TABLA CLIENTES */

CREATE TABLE clientes(
  dni int AUTO_INCREMENT,
  nombre varchar(100), 
  apellidos varchar(100), 
  fecha_nac date,
  tlfno varchar(12),
  PRIMARY KEY (dni)
);

/* CREANDO TABLA PRODUCTOS */

CREATE TABLE productos(
  cod int AUTO_INCREMENT,
  nombre varchar(100), 
  precio float DEFAULT 0,
  PRIMARY KEY (cod)
);

/* CREANDO TABLA PROVEEDOR */

CREATE TABLE proveedor(
  nif int AUTO_INCREMENT,
  nombre varchar(100), 
  dir varchar(100),
  PRIMARY KEY (nif)
);

/* CREANDO TABLA COMPRA */

CREATE TABLE compra(
  dnicliente int,
  codigoproductos int, 
  PRIMARY KEY (dnicliente, codigoproductos),
  CONSTRAINT fkcompraclientes FOREIGN KEY (dnicliente) REFERENCES clientes (dni),
  CONSTRAINT fkcompraproductos FOREIGN KEY (codigoproductos) REFERENCES productos (cod)
);


/* CREANDO TABLA SUMINISTRA */

CREATE TABLE suministra(
  nifproveedor int,
  codproductos int, 
  PRIMARY KEY (nifproveedor, codproductos),
  UNIQUE KEY(nifproveedor),                      /* aqui tenemos unique ey xq el valo debe ser unico , cardinalidad N*/
  CONSTRAINT fksuministraclientes FOREIGN KEY (nifproveedor) REFERENCES clientes (dni),
  CONSTRAINT fksuministraproductos FOREIGN KEY (codproductos) REFERENCES productos (cod)
);














